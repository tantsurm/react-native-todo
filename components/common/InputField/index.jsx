import React from 'react';
// import styled from 'styled-components';

import { View, TextInput, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  root: {},
  label: {},
  input: {}
});

export default ({
  label,
  placeholder,
  input: {
    onChange,
    ...restInputProps
  },
}) => {
  return (
    <View style={styles.root}>
      <Text style={styles.label}>{label}</Text>
      <TextInput
        style={styles.input}
        placeholder={placeholder}
        onChangeText={onChange}
        {...restInputProps} />
    </View>
  )
};
