import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { View, TextInput, Alert } from 'react-native';
import { Field, reduxForm } from 'redux-form';

import InputField from '../../common/InputField';

class SignIn extends Component {
  handleSubmit = (values) => {
    Alert.alert(`Your name is ${values.name} and password is ${values.password}`);
  }

  render() {
    return (
      <View>
        <Field component={InputField} name="name" />
        <Field component={InputField} name="password" />
        <Field component={InputField} name="password_confirmation" />
      </View>
    )
  }

}

export default reduxForm({
  form: 'signUp'
});
