import { lazy } from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Text } from 'react-native';

const SignUp = lazy(() => import('./components/auth/SignUp'));

const AppNavigator = createStackNavigator({
  SignUp: {
    screen: SignUp,
  },
});

export default createAppContainer(AppNavigator);
