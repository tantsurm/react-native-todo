import axios from 'axios';

import {
  createStore, 
  applyMiddleware
} from 'redux';

import combinedReducers from '../reducers';

const store = createStore(combinedReducers, applyMiddleware(thunk, axios));

export default store;
